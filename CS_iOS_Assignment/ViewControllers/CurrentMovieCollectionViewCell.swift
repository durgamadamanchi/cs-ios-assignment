//
//  CurrentMovieCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/25/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class CurrentMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePosterImageView: UIImageView!
    
    func setPosterImage(movie: Movieinfo) {
        
        if let url = URL(string: movie.posterImageURL) {
            self.moviePosterImageView.downloaded(from:url, contentMode: .scaleAspectFit)
        }
    }
}
