//
//  MovieDetailsViewController.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/26/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var posterImageView: UIImageView!
    
    @IBOutlet weak var overview: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var movieDetails: MovieDetails?
    var movieId: Int?
    
    var movieService: MovieServicing? = MovieService(session: URLSession(configuration: .default))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let movieId = movieId else {
            return
        }
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: "Cell")
        movieService?.fetchMovieDetails(movieId: movieId, completion: { [weak self] moviewDetails, error in
            if let moviewDetails = moviewDetails {
                self?.movieDetails = moviewDetails
                self?.updateView()
            }
        })
        
    }
    private func updateView() {
        if let url = self.movieDetails?.backdropImageURL {
            self.posterImageView.downloaded(from: url, contentMode: .scaleAspectFit)
        }
        self.overview.text = self.movieDetails?.overview
        self.tableView.reloadData()
    }
}

extension MovieDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MovieDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        movieDetails?.movieDetails.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if let movieDetails = movieDetails {
            let movieDetailedInfo = movieDetails.movieDetails[indexPath.row]
            cell.textLabel?.text = movieDetailedInfo.title
            cell.detailTextLabel?.text = "\(movieDetailedInfo.value)"
        
        }
        return cell
    }
}

class SubtitleTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .darkGray
        textLabel?.textColor = .white
        detailTextLabel?.textColor = .white
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

