//
//  MoviesListViewController.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class MoviesListViewController: UICollectionViewController {
    
    enum MoviesSection {
        case CurrentlyPlaying
        case Popular
        
    }
    
    static let headerIdentifier = "Movies"
    let moviesHeaderID = "HeaderView"
    var moviesArray: [Movieinfo] = []
    private lazy var dataSource = configureDataSource()
    var currentMoviesArray: [Movieinfo] = []
    var snapshot = NSDiffableDataSourceSnapshot<MoviesSection, Movieinfo>()
    var totalPages = 0
    var currentPages = 1
    var movieService: MovieServicing? = MovieService(session: URLSession(configuration: .default))
    var isLoading = false
    let operationQueue: OperationQueue = OperationQueue()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = dataSource
        configureCollectionView()
        configureHeader()
        loadData()
    }
    
    func configureCollectionView() {
        collectionView.collectionViewLayout = createLayout()
    }
    func configureHeader() {
        dataSource.supplementaryViewProvider = { (
            collectionView: UICollectionView,
            kind: String,
            indexPath: IndexPath) -> UICollectionReusableView? in
            
            let header:HeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: self.moviesHeaderID, for: indexPath) as! HeaderView
            header.label.text = indexPath.section == 0 ?"Playing Now" : "Most Movies"
            return header
        }
    }
    func configureDataSource() -> UICollectionViewDiffableDataSource<MoviesSection, Movieinfo> {
        
        return UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, movieInfo) -> UICollectionViewCell? in
            if indexPath.section == 0  {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CurrentMovieCollectionViewCell", for: indexPath) as? CurrentMovieCollectionViewCell else {
                    return UICollectionViewCell()
                }
                cell.setPosterImage(movie: movieInfo)
                return cell
            }
            else {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularMovieCollectionViewCell", for: indexPath) as? PopularMovieCollectionViewCell else {
                    return UICollectionViewCell()
                }
                cell.configure(movieInfo: movieInfo)
                return cell
            }
            
        })
    }
    
    func loadData()  {
        movieService?.fetchCurrentlyPlayingMovies(completion: { [weak self](movies, error) in
            guard let movies = movies, let strongSelf = self else {
                return
            }
            
            if(!strongSelf.snapshot.sectionIdentifiers.contains(MoviesSection.CurrentlyPlaying)) { strongSelf.snapshot.appendSections([MoviesSection.CurrentlyPlaying])
            }
            
            strongSelf.snapshot.appendItems(movies.results, toSection: .CurrentlyPlaying)
            strongSelf.dataSource.apply(strongSelf.snapshot, animatingDifferences: true)
            
        })
        
        movieService?.fetchMostPopularMovies(page: currentPages, completion: { [weak self](movies, error) in
            guard let moviesList = movies, let strongSelf = self else {
                return
            }
            if(!strongSelf.snapshot.sectionIdentifiers.contains(MoviesSection.Popular)) {             strongSelf.snapshot.appendSections([MoviesSection.Popular])
            }
            strongSelf.totalPages = moviesList.total_pages
            strongSelf.snapshot.appendItems(moviesList.results, toSection: .Popular)
            strongSelf.dataSource.apply(strongSelf.snapshot, animatingDifferences: true)
        })
    }
    
    private func loadMoreMovies(page: Int) {
        if operationQueue.operations.count != 0 {
            return
        }
        let operation = BlockOperation() { [weak self] in
            
            guard let strongSelf = self else {
                return
            }
            strongSelf.isLoading = false
            
            strongSelf.movieService?.fetchMostPopularMovies(page: page, completion: { [weak self](movies, error) in
                guard let moviesList = movies, let strongSelf = self else {
                    return
                }
                if(!strongSelf.snapshot.sectionIdentifiers.contains(MoviesSection.Popular)) {             strongSelf.snapshot.appendSections([MoviesSection.Popular])
                }
                strongSelf.snapshot.appendItems(moviesList.results, toSection: .Popular)
                strongSelf.dataSource.apply(strongSelf.snapshot, animatingDifferences: true)
                strongSelf.isLoading = false
            })
        }
        operationQueue.addOperation(operation)
    }
    
    private func createLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout { sectionNumber,env in
            if sectionNumber == 0 {
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                let groupSize = NSCollectionLayoutSize(widthDimension: .absolute(140), heightDimension: .absolute(214))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(25))
                let header = NSCollectionLayoutBoundarySupplementaryItem(
                    layoutSize: headerSize,
                    elementKind:  UICollectionView.elementKindSectionHeader, alignment: .top)
                section.boundarySupplementaryItems = [header]
                section.interGroupSpacing = 0
                section.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 0, bottom: 2, trailing: 0)
                section.orthogonalScrollingBehavior = .continuous
                return section
            }
            else {
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),heightDimension: .absolute(120))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(20))
                let header = NSCollectionLayoutBoundarySupplementaryItem(
                    layoutSize: headerSize,
                    elementKind:  UICollectionView.elementKindSectionHeader, alignment: .top)
                section.boundarySupplementaryItems = [header]
                section.interGroupSpacing = 5
                section.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 0, bottom: 2, trailing: 0)
                return section
            }
        }
    }
}

extension MoviesListViewController {
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let offsetY = collectionView.contentOffset.y
        let contentHeight = collectionView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height && !self.isLoading && currentPages < totalPages {
            currentPages += 1
            loadMoreMovies(page: currentPages)
        }
        
    }
}
extension MoviesListViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let movieInfo = snapshot.itemIdentifiers(inSection: .Popular)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailsViewController") as? MovieDetailsViewController {
                viewController.movieId = movieInfo[indexPath.row].id
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

class HeaderView: UICollectionReusableView {
    @IBOutlet weak var label:UILabel!
}
