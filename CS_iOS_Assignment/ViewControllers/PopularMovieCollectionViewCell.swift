//
//  PopularMovieCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/27/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class PopularMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var ratingView: CircularRatingsView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    func configure(movieInfo: Movieinfo) {
        title.text = movieInfo.original_title
        releaseDate.text = movieInfo.release_date
        if let url = URL(string: movieInfo.posterImageURL) {
            poster.downloaded(from:url, contentMode: .scaleAspectFit)
        }
        ratingView.labelText = String(movieInfo.vote_average)
        
        let progressPercentage = CGFloat(movieInfo.vote_average) / CGFloat(10.0)
        ratingView.setProgress(progressPercentage: progressPercentage)
    }
}
