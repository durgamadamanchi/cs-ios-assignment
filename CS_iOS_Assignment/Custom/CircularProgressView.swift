//
//  CircularRatingsView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class CircularRatingsView: UIView {
    
    private var titleLabel = UILabel()
    private let foregroundLayer = CAShapeLayer()
    private let backgroundLayer = CAShapeLayer()
    private var layoutDone = false
    
    private var pathCenter: CGPoint {
        get {
            return self.convert(self.center, from: self.superview)
        }
    }
    
    public var lineWidth: CGFloat = 5.0 {
        didSet {
            foregroundLayer.lineWidth = lineWidth
            backgroundLayer.lineWidth = lineWidth - (0.20 * lineWidth)
        }
    }
    
    public var labelSize: CGFloat = 15.0 {
        didSet {
            titleLabel.font = UIFont.systemFont(ofSize: 20)
            titleLabel.sizeToFit()
            configureLabel()
        }
    }
    
    public var labelText: String = "-" {
        didSet {
            titleLabel.text = labelText
            titleLabel.sizeToFit()
        }
    }
    
    private var radius: CGFloat {
        get {
            let result = self.frame.width < self.frame.height ?  (self.frame.width - lineWidth) / 2 :  (self.frame.height - lineWidth) / 2
            return result
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = labelText
        setUpCircularView()
    }
    
    override func layoutSublayers(of layer: CALayer) {
        if !layoutDone {
            let tempText = titleLabel.text
            setUpCircularView()
            titleLabel.text = tempText
            layoutDone = true
        }
    }
    
    public func setUpCircularView() {
        self.layer.sublayers = nil
        
        drawBackgroundLayer()
        drawForegroundLayer()
        self.addSubview(titleLabel)
        titleLabel.text = labelText
    }
    
    private func drawBackgroundLayer() {
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        backgroundLayer.path = path.cgPath
        backgroundLayer.strokeColor = UIColor.gray.cgColor
        backgroundLayer.lineWidth = lineWidth - (lineWidth * 20 / 100)
        backgroundLayer.fillColor = UIColor.white.cgColor
        self.layer.addSublayer(backgroundLayer)
    }
    
    private func drawForegroundLayer() {
        
        let startAngle = (-CGFloat.pi / 2)
        let endAngle = 2 * CGFloat.pi + startAngle
        
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        foregroundLayer.lineCap = CAShapeLayerLineCap.round
        foregroundLayer.path = path.cgPath
        foregroundLayer.lineWidth = lineWidth
        foregroundLayer.fillColor = UIColor.clear.cgColor
        foregroundLayer.strokeStart = 0
        
        self.layer.addSublayer(foregroundLayer)
    }
    
    private func configureLabel() {
        titleLabel.sizeToFit()
        titleLabel.center = pathCenter
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.textColor = UIColor.black
    }
    
    public func setProgress(progressPercentage: CGFloat) {
        
        var progress: CGFloat {
            get {
                switch progressPercentage {
                case let progressPercentage where progressPercentage > 1: return 1
                case let progressPercentage where progressPercentage < 0: return 0
                default: return progressPercentage
                }
            }
        }
        let color =  progressPercentage > 0.5 ? UIColor.green.cgColor : UIColor.yellow.cgColor
        titleLabel.text = labelText
        configureLabel()
        foregroundLayer.strokeColor = color
        foregroundLayer.strokeEnd = progress
    }
}
