//
//  ImageCache.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/28/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

public protocol ImageCaching: AnyObject {
    func getImage(forKey key: URL) -> UIImage?
    func insertImage(object: UIImage?, forKey key: URL)
    func removeImage(for key: URL)
    func removeAllImages()
}

final class ImageCache {
    private lazy var imageCache: NSCache<AnyObject, AnyObject> = {
        let cache = NSCache<AnyObject, AnyObject>()
        cache.countLimit = config.countLimit
        return cache
    }()
    private var memoryObserver: NSObjectProtocol?
    
    private let lock = NSLock()
    private let config: Config
    
    struct Config {
        let countLimit: Int
        let memoryLimit: Int
        
        static let defaultConfig = Config(countLimit: 100, memoryLimit: 1024 * 1024 * 100) // 100 MB
    }
    init(config: Config = Config.defaultConfig) {
        self.config = config
        addMemoryWarningObserver()
        
    }
    
    deinit {
        self.removeAllImages()
        removeMemoryWarningObserver()
    }
}

extension ImageCache {
    func addMemoryWarningObserver() {
        memoryObserver = NotificationCenter.default.addObserver(
            forName: UIApplication.didReceiveMemoryWarningNotification,
            object: nil,
            queue: nil
        ) { [weak self] _ in
            self?.removeAllImages()
        }
    }
    
    func removeMemoryWarningObserver() {
        guard let memoryObserver = memoryObserver  else {
            return
        }
        self.memoryObserver = nil
        NotificationCenter.default.removeObserver(memoryObserver)
    }
}
extension ImageCache: ImageCaching {
    func getImage(forKey key: URL) -> UIImage? {
        return imageCache.object(forKey: key as AnyObject) as? UIImage
    }

    func insertImage(object: UIImage?, forKey key: URL) {
        guard let image = object else { return removeImage(for: key) }

        lock.lock(); defer { lock.unlock() }
        imageCache.setObject(image, forKey: key as AnyObject)
    }
    
    func removeImage(for key: URL) {
        lock.lock(); defer { lock.unlock() }
        imageCache.removeObject(forKey: key as AnyObject)
    }
    
    func removeAllImages() {
        imageCache.removeAllObjects()
    }
}
