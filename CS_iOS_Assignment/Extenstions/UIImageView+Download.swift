//
//  UIImageView+Download.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/24/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

let imageCache = ImageCache()

extension  UIImageView {

    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        // retrieves image if already available in cache
        if let imageFromCache = imageCache.getImage(forKey: url) {
            DispatchQueue.main.async() {
                self.image = imageFromCache
            }
        }
        else {
            contentMode = mode
            DispatchQueue.global().async {
                URLSession.shared.dataTask(with: url) { data, response, error in
                    guard
                        let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                        let data = data, error == nil,
                        let image = UIImage(data: data)
                        else { return }
                    imageCache.insertImage(object: image, forKey: url)
                    DispatchQueue.main.async() {
                        self.image = image
                    }
                }.resume()
            }
        }
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

