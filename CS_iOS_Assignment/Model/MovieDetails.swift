//
//  MovieDetails.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/26/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct MovieDetails: Decodable {
    let original_title: String
    let overview: String
    let genres : [Gener]
    let release_date: String
    let runtime : Float
    let backdrop_path: String
}

extension MovieDetails {
    var allGeners: String {
        var allGenre = ""
        genres.forEach { genre in
            if !allGenre.isEmpty {
                allGenre.append(" / ")
            }
            allGenre.append(genre.name)
        }
        return allGenre
    }
    var backdropImageURL:String {
        return "https://image.tmdb.org/t/p/w500" + backdrop_path
    }
    
    var movieDetails: [MovieDetailedInfo] {
        return [
            MovieDetailedInfo(title: "Durtion", value: runtime),
            MovieDetailedInfo(title: "Title", value: original_title),
            MovieDetailedInfo(title: "Release_date", value: release_date),
            MovieDetailedInfo(title: "Genres", value: allGeners)
        ]
    }
}
struct MovieDetailedInfo {
    let title: String
    let value: Any
}
struct Gener: Decodable {
    let id: NSInteger
    let name: String
}
