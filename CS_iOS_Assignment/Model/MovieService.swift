//
//  MovieService.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import Foundation

public enum ServiceError: Error {
    case apiError
    case invalidEndpoint
    case invalidResponse
    case noData
    case decodeError
    case invalidInput
}

extension ServiceError {
    public var errorDescription: String? {
        switch self {
        case .apiError,.invalidEndpoint,.invalidResponse:
            return "Encountered a problem while fetching movies information."
        case .noData:
            return "There is no data available"
            
        case .decodeError:
            return "Encountered a problem while fetching movies information."
            
        case .invalidInput:
            return "Encountered a problem while fetching movies information."
        }
    }
}

enum Constants {
    static let CurrentlyPlayingMoviesAPIURL = "https://api.themoviedb.org/3/movie/now_playing"
    static let MostPopularMoviesAPIURL = "https://api.themoviedb.org/3/movie/popular"
    static let MovieDetailsAPIURL = "https://api.themoviedb.org/3/movie/"
}

protocol MovieServicing {
    func fetchCurrentlyPlayingMovies(completion: @escaping (MoviesList?, ServiceError?) -> Void)
    func fetchMostPopularMovies(page: Int, completion: @escaping (MoviesList?, ServiceError?) -> Void)
    func fetchMovieDetails(movieId: Int, completion: @escaping (MovieDetails?, ServiceError?) -> Void)
}

class MovieService {
    private let apiId = "55957fcf3ba81b137f8fc01ac5a31fb5"
    private let session: URLSession
    private var dataTask: URLSessionDataTask?
    init(session: URLSession) {
        self.session = session
    }
}

extension MovieService: MovieServicing {
    
    func fetchCurrentlyPlayingMovies(completion: @escaping (MoviesList?, ServiceError?) -> Void) {
        
        guard var urlComponents = URLComponents(string: Constants.CurrentlyPlayingMoviesAPIURL) else {
            completion(nil,.invalidEndpoint)
            return
        }
        
        let queryItems = [URLQueryItem(name: "language", value: "en-US"),
                          URLQueryItem(name: "page", value: "undefined"),
                          URLQueryItem(name: "api_key", value: apiId)]
        
        urlComponents.queryItems = queryItems
        // URL should not be nil to proceed further
        guard let url = urlComponents.url  else {
            completion(nil,.invalidEndpoint)
            return
        }
        
        fetchData(url: url, completion: completion)
    }
    
    func fetchMostPopularMovies(page: Int, completion: @escaping (MoviesList?, ServiceError?) -> Void) {
        
        guard var urlComponents = URLComponents(string: Constants.MostPopularMoviesAPIURL) else {
            completion(nil,.invalidEndpoint)
            return
        }
        
        let queryItems = [URLQueryItem(name: "language", value: "en-US"),
                          URLQueryItem(name: "page", value: "\(page)"),
                          URLQueryItem(name: "api_key", value: apiId)]
        
        urlComponents.queryItems = queryItems
        // URL should not be nil to proceed further
        guard let url = urlComponents.url  else {
            completion(nil,.invalidEndpoint)
            return
        }
        fetchData(url: url, completion: completion)
    }
    
    func fetchMovieDetails(movieId: Int, completion: @escaping (MovieDetails?, ServiceError?) -> Void) {
        
        guard var urlComponents = URLComponents(string:Constants.MovieDetailsAPIURL+String(movieId)) else {
            completion(nil,.invalidEndpoint)
            return
        }
        
        let queryItems = [URLQueryItem(name: "language", value: "en-US"),
                          URLQueryItem(name: "api_key", value: apiId)]
        
        urlComponents.queryItems = queryItems
        // URL should not be nil to proceed further
        guard let url = urlComponents.url  else {
            completion(nil,.invalidEndpoint)
            return
        }
        fetchData(url: url, completion: completion)
    }
    
    private func fetchData<T: Decodable>(url:URL, completion: @escaping (T?,ServiceError?) -> ()) {
        dataTask = session.dataTask(with: url) { data, response, error in
            let dispatchQueue = DispatchQueue.main

            // check for error
            guard error == nil else {
                dispatchQueue.async {
                    completion(nil, .noData)
                }
                return
            }
            // check for data
            guard let data = data else {
                dispatchQueue.async {
                    completion(nil, .noData)
                }
                return
            }
            // decode the response
            do {
                let movieDetails = try JSONDecoder().decode(T.self, from: data)
                dispatchQueue.async {
                    completion(movieDetails, nil)
                }
            }
            catch {
                if (try? JSONDecoder().decode(ErrorResults.self, from: data)) != nil {
                    dispatchQueue.async {
                        completion(nil,.invalidInput)
                    }
                } else {
                    dispatchQueue.async {
                        completion(nil,.decodeError)
                    }
                }
            }
        }
        dataTask?.resume()
    }
}
