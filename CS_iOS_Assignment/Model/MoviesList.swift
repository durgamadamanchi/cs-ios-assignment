//
//  MoviesList.swift
//  CS_iOS_Assignment
//
//  Created by Durga Madamanchi on 6/24/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation


struct MoviesList: Decodable {
    let total_pages: Int
    let results: [Movieinfo]
}

struct Movieinfo: Decodable, Hashable {
    let id: Int
    let poster_path: String
    let original_title: String
    let vote_average: Float
    let overview: String
    let release_date: String
    let popularity: Float
    
    var posterImageURL:String {
        return "https://image.tmdb.org/t/p/w500" + poster_path
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    static func == (lhs: Movieinfo, rhs: Movieinfo) -> Bool {
        return lhs.id == rhs.id
    }
}

struct ErrorResults: Decodable {
    let status_code: Int
    let status_message: String
    let success: Bool
}
