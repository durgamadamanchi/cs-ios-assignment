# iOS Assignment CS

## Architecture

I felt MVC is the best suit to this project. Because the screens are less and there are very less data conversions. 

## ImageCaching

ImageCache: I implemented all CRUD operations for ImageCaching. It limits the cache size with the maximum number of objects and the total cost, such as the size in bytes of all images. It uses NSLock instance to provide mutually exclusive access and make the cache thread-safe. Also it  observes "didReceiveMemoryWarningNotification" to clear the cached memory when ever needed.

## CustomProgressBar

CircularRatingsView: It is straight forward implementation. It has two layers. one is foreground which is for actual progress bar and the other is background for the stroke line.


## Other:
It is Clean code.
Implemented loosely coupled code.
Followed Protocol oriented programing.
No violating SOLID Principles.
Easy to write unit tests by mocking the data.

