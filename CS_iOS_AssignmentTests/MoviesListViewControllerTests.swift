//
//  MoviesListViewControllerTests.swift
//  CS_iOS_AssignmentTests
//
//  Created by Durga Madamanchi on 6/28/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment

class MoviesListViewControllerTests: XCTestCase {
    
    var viewController: MoviesListViewController!
    var serviceMock = MovieServiceMock()
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyboard.instantiateViewController(withIdentifier: "MoviesListViewController") as? MoviesListViewController
        viewController.movieService = serviceMock
    }
    
    override func tearDown() {
        viewController = nil
    }
    
    func testViewControllerNotNil() {
        viewController.viewDidLoad()
        XCTAssertNotNil(viewController)
        XCTAssertNotNil(viewController.collectionView)
        XCTAssertNotNil(viewController.snapshot)
        XCTAssertNotNil(viewController.collectionView.collectionViewLayout)
    }
    
    func testCurrentMovieCollectionViewCell() {
        let cellIdentifier = "CurrentMovieCollectionViewCell"
        let cell = viewController.collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: IndexPath(row: 0, section: 0)) as? CurrentMovieCollectionViewCell
        XCTAssertNotNil(cell)
    }
    
    func testPopularMovieCollectionViewCell() {
        let cellIdentifier = "PopularMovieCollectionViewCell"
        let cell = viewController.collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: IndexPath(row: 0, section: 1)) as? PopularMovieCollectionViewCell
        XCTAssertNotNil(cell)
    }
    
    func testNumOfSection() {
        serviceMock.hasResponse = true
        viewController.viewDidLoad()
        XCTAssertEqual(viewController.snapshot.numberOfSections, 2)
    }
    
    func testNumOfRowsPupularMoviesInSection() {
        serviceMock.hasResponse = true
        viewController.viewDidLoad()
        XCTAssertEqual(viewController.snapshot.numberOfItems(inSection: .Popular), 20)
    }
}
