//
//  MovieServiceTests.swift
//  CS_iOS_AssignmentTests
//
//  Created by Durga Madamanchi on 6/25/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import XCTest
import UIKit
@testable import CS_iOS_Assignment

class MovieServiceTests: XCTestCase {

    func test_MoviesList_PopularMovies_ExpectedURLHostAndPath() {
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        moviesService.fetchMostPopularMovies(page: 1) { (movies, error) in
            XCTAssertEqual(mockURLSession.cachedUrl?.host, "api.themoviedb.org")
            XCTAssertEqual(mockURLSession.cachedUrl?.path,"/3/movie/popular")
        }
    }
    
    func test_MoviesList_CurrentMovies_ExpectedURLHostAndPath() {
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        moviesService.fetchCurrentlyPlayingMovies { (movies, error) in
            XCTAssertEqual(mockURLSession.cachedUrl?.host, "api.themoviedb.org")
            XCTAssertEqual(mockURLSession.cachedUrl?.path,"/3/movie/now_playing")
        }
    }
    
    func test_MovieDetails_ExpectedURLHostAndPath() {
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        moviesService.fetchMovieDetails(movieId: 353) { (movies, error) in
            XCTAssertEqual(mockURLSession.cachedUrl?.host, "api.themoviedb.org")
            XCTAssertEqual(mockURLSession.cachedUrl?.path,"/3/movie/353")
        }
    }

    func test_MoviesList_ReturnsSuccessResponse() {
        let jsonData = mockDataForMoviesList.data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        let popularMoviesExpectation = expectation(description: "PopularMovies")
        var popularMoviesResponse: MoviesList?
        moviesService.fetchMostPopularMovies(page: 1){ moviesInfo, error in
            popularMoviesResponse = moviesInfo
            popularMoviesExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(popularMoviesResponse)
        }
    }
    
    func test_MoviesList_ReturnsFailResponse() {
        let jsonData = mockDataForFailResponse.data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        let popularMoviesExpectation = expectation(description: "FailPopularMovies")
        var errorResponse: ServiceError?
        moviesService.fetchMostPopularMovies(page: 1){ moviesInfo, error in
            errorResponse = error
            popularMoviesExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    
    func test_MoviesList_ReturnsInvalidResponse() {
        let jsonData = "{}".data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        let popularMoviesExpectation = expectation(description: "PopularMovies")
        var errorResponse: ServiceError?
        moviesService.fetchMostPopularMovies(page: 1){ moviesInfo, error in
            errorResponse = error
            popularMoviesExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    
    func test_CrunntlyPlaying_MoviesList_ReturnsInvalidResponse() {
        let jsonData = "{}".data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        let popularMoviesExpectation = expectation(description: "PopularMovies")
        var errorResponse: ServiceError?
        moviesService.fetchCurrentlyPlayingMovies { moviesInfo, error in
            errorResponse = error
            popularMoviesExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    
    func test_MovieDetails_ReturnsInvalidResponse() {
        let jsonData = mockDataForDetailResponse.data(using: .utf8)
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
        let moviesService = MovieService(session: mockURLSession)
        let popularMoviesExpectation = expectation(description: "MovieDetails")
        var response: MovieDetails?
        moviesService.fetchMovieDetails(movieId: 345) { moviesInfo, error in
            response = moviesInfo
            popularMoviesExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(response)
        }
    }
}
