//
//  MovieServiceMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Durga Madamanchi on 6/25/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
@testable import CS_iOS_Assignment

final class MovieServiceMock {
    var hasResponse = false
}
extension MovieServiceMock: MovieServicing {
    func fetchMovieDetails(movieId: Int, completion: @escaping (MovieDetails?, ServiceError?) -> Void) {
    }
    func fetchMostPopularMovies(page: Int, completion: @escaping (MoviesList?, ServiceError?) -> Void) {
        if hasResponse {
            let mockResponse = MockMoviesListData()
            completion(mockResponse.getMockData(), nil)
        }
        else {
            completion(nil, .noData)
        }
    }
    func fetchCurrentlyPlayingMovies(completion: @escaping (MoviesList?, ServiceError?) -> Void) {
        if hasResponse {
            let mockResponse = MockMoviesListData()
            completion(mockResponse.getMockData(), nil)
        }
        else {
            completion(nil, .noData)
        }
    }
}
