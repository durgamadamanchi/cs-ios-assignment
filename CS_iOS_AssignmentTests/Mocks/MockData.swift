//
//  MockData.swift
//  CS_iOS_AssignmentTests
//
//  Created by Durga Madamanchi on 6/25/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

@testable import CS_iOS_Assignment

let mockDataForMoviesList = """
{
    "total_pages": 500,
   "results":[
      {
         "popularity":78.879,
         "vote_count":2298,
         "video":false,
         "poster_path":"/f4aul3FyD3jv3v4bul1IrkWZvzq.jpg",
         "id":508439,
         "adult":false,
         "backdrop_path":"/dW6yBuKwiMeronJZw8kozYLMorB.jpg",
         "original_language":"en",
         "original_title":"Onward",
         "genre_ids":[
            12,
            16,
            35,
            14,
            10751
         ],
         "title":"Onward",
         "vote_average":7.9,
         "overview":"In a suburban fantasy world, two teenage elf brothers embark on an extraordinary quest to discover if there is still a little magic left out there.",
         "release_date":"2020-02-29"
      },
      {
         "popularity":65.654,
         "id":451184,
         "video":false,
         "vote_count":67,
         "vote_average":6.5,
         "title":"Wasp Network",
         "release_date":"2020-01-29",
         "original_language":"en",
         "original_title":"Wasp Network",
         "genre_ids":[
            18,
            36,
            53
         ],
         "backdrop_path":"/72r4uAQGsa8KEv0DB2TpSu31lEB.jpg",
         "adult":false,
         "overview":"Havana, Cuba, 1990. René González, an airplane pilot, unexpectedly flees the country, leaving behind his wife Olga and his daughter Irma, and begins a new life in Miami, where he becomes a member of an anti-Castro organization.",
         "poster_path":"/fOvqEunubL3wPskvtk2Ficfl0pH.jpg"
      },
      {
         "popularity":64.572,
         "vote_count":1479,
         "video":false,
         "poster_path":"/jtrhTYB7xSrJxR1vusu99nvnZ1g.jpg",
         "id":522627,
         "adult":false,
         "backdrop_path":"/tintsaQ0WLzZsTMkTiqtMB3rfc8.jpg",
         "original_language":"en",
         "original_title":"The Gentlemen",
         "genre_ids":[
            28,
            35,
            80
         ],
         "title":"The Gentlemen",
         "vote_average":7.7,
         "overview":"American expat Mickey Pearson has built a highly profitable marijuana empire in London. When word gets out that he’s looking to cash out of the business forever it triggers plots, schemes, bribery and blackmail in an attempt to steal his domain out from under him.",
         "release_date":"2019-12-03"
      },
      {
         "popularity":55.071,
         "vote_count":225,
         "video":false,
         "poster_path":"/oyG9TL7FcRP4EZ9Vid6uKzwdndz.jpg",
         "id":696374,
         "adult":false,
         "backdrop_path":"/fDTPiqCynPQIkojfzdeyRHpw99S.jpg",
         "original_language":"en",
         "original_title":"Gabriel's Inferno",
         "genre_ids":[
            10749
         ],
         "title":"Gabriel's Inferno",
         "vote_average":8.8,
         "overview":"An intriguing and sinful exploration of seduction, forbidden love, and redemption, Gabriel's Inferno is a captivating and wildly passionate tale of one man's escape from his own personal hell as he tries to earn the impossible--forgiveness and love.",
         "release_date":"2020-05-29"
      },
      {
         "popularity":54.295,
         "vote_count":2686,
         "video":false,
         "poster_path":"/8WUVHemHFH2ZIP6NWkwlHWsyrEL.jpg",
         "id":338762,
         "adult":false,
         "backdrop_path":"/lP5eKh8WOcPysfELrUpGhHJGZEH.jpg",
         "original_language":"en",
         "original_title":"Bloodshot",
         "genre_ids":[
            28,
            878
         ],
         "title":"Bloodshot",
         "vote_average":7,
         "overview":"After he and his wife are murdered, marine Ray Garrison is resurrected by a team of scientists. Enhanced with nanotechnology, he becomes a superhuman, biotech killing machine—'Bloodshot'. As Ray first trains with fellow super-soldiers, he cannot recall anything from his former life. But when his memories flood back and he remembers the man that killed both him and his wife, he breaks out of the facility to get revenge, only to discover that there's more to the conspiracy than he thought.",
         "release_date":"2020-03-05"
      },
      {
         "popularity":52.961,
         "vote_count":11,
         "video":false,
         "poster_path":"/aINpljdt3VVMrCLtJW4BektwYOp.jpg",
         "id":522098,
         "adult":false,
         "backdrop_path":"/cjAirCV9TyTQcp7mFNRnvgkoVFV.jpg",
         "original_language":"en",
         "original_title":"Babyteeth",
         "genre_ids":[
            35,
            18
         ],
         "title":"Babyteeth",
         "vote_average":4.4,
         "overview":"A terminally ill teen upsets her parents when she falls in love with a small-time drug dealer.",
         "release_date":"2020-06-18"
      },
      {
         "popularity":52.549,
         "vote_count":972,
         "video":false,
         "poster_path":"/wxPhn4ef1EAo5njxwBkAEVrlJJG.jpg",
         "id":514847,
         "adult":false,
         "backdrop_path":"/naXUDz0VGK7aaPlEpsuYW8kNVsr.jpg",
         "original_language":"en",
         "original_title":"The Hunt",
         "genre_ids":[
            28,
            27,
            53
         ],
         "title":"The Hunt",
         "vote_average":6.7,
         "overview":"Twelve strangers wake up in a clearing. They don't know where they are—or how they got there. In the shadow of a dark internet conspiracy theory, ruthless elitists gather at a remote location to hunt humans for sport. But their master plan is about to be derailed when one of the hunted turns the tables on her pursuers.",
         "release_date":"2020-03-11"
      },
      {
         "popularity":46.302,
         "vote_count":1047,
         "video":false,
         "poster_path":"/jHo2M1OiH9Re33jYtUQdfzPeUkx.jpg",
         "id":385103,
         "adult":false,
         "backdrop_path":"/fKtYXUhX5fxMxzQfyUcQW9Shik6.jpg",
         "original_language":"en",
         "original_title":"Scoob!",
         "genre_ids":[
            12,
            16,
            35,
            9648,
            10751
         ],
         "title":"Scoob!",
         "vote_average":8,
         "overview":"In Scooby-Doo’s greatest adventure yet, see the never-before told story of how lifelong friends Scooby and Shaggy first met and how they joined forces with young detectives Fred, Velma, and Daphne to form the famous Mystery Inc. Now, with hundreds of cases solved, Scooby and the gang face their biggest, toughest mystery ever: an evil plot to unleash the ghost dog Cerberus upon the world. As they race to stop this global “dogpocalypse,” the gang discovers that Scooby has a secret legacy and an epic destiny greater than anyone ever imagined.",
         "release_date":"2020-05-15"
      },
      {
         "popularity":43.983,
         "vote_count":402,
         "video":false,
         "poster_path":"/myf3qzpeN0JbuFRPwSpJcz7rmAT.jpg",
         "id":458305,
         "adult":false,
         "backdrop_path":"/jZ4YdDCnfK4cO13do9J4JaT8d2O.jpg",
         "original_language":"en",
         "original_title":"Vivarium",
         "genre_ids":[
            27,
            878,
            53
         ],
         "title":"Vivarium",
         "vote_average":5.8,
         "overview":"A young woman and her fiancé are in search of the perfect starter home. After following a mysterious real estate agent to a new housing development, the couple finds themselves trapped in a maze of identical houses and forced to raise an otherworldly child.",
         "release_date":"2019-07-12"
      },
      {
         "popularity":43.651,
         "vote_count":1326,
         "video":false,
         "poster_path":"/gzlbb3yeVISpQ3REd3Ga1scWGTU.jpg",
         "id":443791,
         "adult":false,
         "backdrop_path":"/ww7eC3BqSbFsyE5H5qMde8WkxJ2.jpg",
         "original_language":"en",
         "original_title":"Underwater",
         "genre_ids":[
            28,
            27,
            878,
            53
         ],
         "title":"Underwater",
         "vote_average":6.4,
         "overview":"After an earthquake destroys their underwater station, six researchers must navigate two miles along the dangerous, unknown depths of the ocean floor to make it to safety in a race against time.",
         "release_date":"2020-01-08"
      },
      {
         "popularity":40.094,
         "vote_count":1449,
         "video":false,
         "poster_path":"/33VdppGbeNxICrFUtW2WpGHvfYc.jpg",
         "id":481848,
         "adult":false,
         "backdrop_path":"/9sXHqZTet3Zg5tgcc0hCDo8Tn35.jpg",
         "original_language":"en",
         "original_title":"The Call of the Wild",
         "genre_ids":[
            12,
            18,
            10751
         ],
         "title":"The Call of the Wild",
         "vote_average":7.4,
         "overview":"Buck is a big-hearted dog whose blissful domestic life is turned upside down when he is suddenly uprooted from his California home and transplanted to the exotic wilds of the Yukon during the Gold Rush of the 1890s. As the newest rookie on a mail delivery dog sled team—and later its leader—Buck experiences the adventure of a lifetime, ultimately finding his true place in the world and becoming his own master.",
         "release_date":"2020-02-19"
      },
      {
         "popularity":36.911,
         "vote_count":188,
         "video":false,
         "poster_path":"/ygCQnDEqUEIamBpdQdDYnFfxvgM.jpg",
         "id":339095,
         "adult":false,
         "backdrop_path":"/t93doi7EzoqLFckidrGGnukFPwd.jpg",
         "original_language":"en",
         "original_title":"The Last Days of American Crime",
         "genre_ids":[
            28,
            80,
            53
         ],
         "title":"The Last Days of American Crime",
         "vote_average":5.6,
         "overview":"In the not-too-distant future, as a final response to crime and terrorism, the U.S. government plans to broadcast a signal that will make it impossible for anyone to knowingly break the law.",
         "release_date":"2020-06-05"
      },
      {
         "popularity":35.57,
         "vote_count":17249,
         "video":false,
         "poster_path":"/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg",
         "id":603,
         "adult":false,
         "backdrop_path":"/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg",
         "original_language":"en",
         "original_title":"The Matrix",
         "genre_ids":[
            28,
            878
         ],
         "title":"The Matrix",
         "vote_average":8.1,
         "overview":"Set in the 22nd century, The Matrix tells the story of a computer hacker who joins a group of underground insurgents fighting the vast and powerful computers who now rule the earth.",
         "release_date":"1999-03-30"
      },
      {
         "popularity":35.277,
         "vote_count":160,
         "video":false,
         "poster_path":"/suORidtGKPO6tWwNqiwGvNo85z3.jpg",
         "id":565743,
         "adult":false,
         "backdrop_path":"/lVSMgZUBE4XTVaPBEBAczre4f1W.jpg",
         "original_language":"en",
         "original_title":"The Vast of Night",
         "genre_ids":[
            18,
            9648,
            878
         ],
         "title":"The Vast of Night",
         "vote_average":6.6,
         "overview":"At the dawn of the space-race, two radio-obsessed teens discover a strange frequency over the airwaves in what becomes the most important night of their lives and in the history of their small town.",
         "release_date":"2020-05-15"
      },
      {
         "popularity":35.095,
         "vote_count":16,
         "video":false,
         "poster_path":"/qWrPZZL7CC3MqySlOzpXaUWjUrd.jpg",
         "id":698783,
         "adult":false,
         "backdrop_path":"/yOaOwFJMOFJa50hGJEgqWAvn2gO.jpg",
         "original_language":"en",
         "original_title":"2 Minutes of Fame",
         "genre_ids":[
            35
         ],
         "title":"2 Minutes of Fame",
         "vote_average":7.2,
         "overview":"An up-and-coming stand-up comic moves to L.A. to pursue a film career after video clips of his act make him an online sensation.",
         "release_date":"2020-06-16"
      },
      {
         "popularity":31.641,
         "vote_count":3316,
         "video":false,
         "poster_path":"/8ZX18L5m6rH5viSYpRnTSbb9eXh.jpg",
         "id":619264,
         "adult":false,
         "backdrop_path":"/3tkDMNfM2YuIAJlvGO6rfIzAnfG.jpg",
         "original_language":"es",
         "original_title":"El hoyo",
         "genre_ids":[
            18,
            878,
            53
         ],
         "title":"The Platform",
         "vote_average":7,
         "overview":"A mysterious place, an indescribable prison, a deep hole. An unknown number of levels. Two inmates living on each level. A descending platform containing food for all of them. An inhuman fight for survival, but also an opportunity for solidarity…",
         "release_date":"2019-11-08"
      },
      {
         "popularity":30.441,
         "vote_count":465,
         "video":false,
         "poster_path":"/tIpGQ9uuII7QVFF0GHCFTJFfXve.jpg",
         "id":555974,
         "adult":false,
         "backdrop_path":"/rpGYHowXtjw37UxdwO1ZcK5E8IN.jpg",
         "original_language":"en",
         "original_title":"Brahms: The Boy II",
         "genre_ids":[
            27,
            9648,
            53
         ],
         "title":"Brahms: The Boy II",
         "vote_average":6.3,
         "overview":"After a family moves into the Heelshire Mansion, their young son soon makes friends with a life-like doll called Brahms.",
         "release_date":"2020-02-20"
      },
      {
         "popularity":30.067,
         "vote_count":580,
         "video":false,
         "poster_path":"/c01Y4suApJ1Wic2xLmaq1QYcfoZ.jpg",
         "id":618344,
         "adult":false,
         "backdrop_path":"/sQkRiQo3nLrQYMXZodDjNUJKHZV.jpg",
         "original_language":"en",
         "original_title":"Justice League Dark: Apokolips War",
         "genre_ids":[
            28,
            12,
            16,
            14,
            878
         ],
         "title":"Justice League Dark: Apokolips War",
         "vote_average":8.5,
         "overview":"Earth is decimated after intergalactic tyrant Darkseid has devastated the Justice League in a poorly executed war by the DC Super Heroes. Now the remaining bastions of good – the Justice League, Teen Titans, Suicide Squad and assorted others – must regroup, strategize and take the war to Darkseid in order to save the planet and its surviving inhabitants.",
         "release_date":"2020-05-05"
      },
      {
         "popularity":27.422,
         "vote_count":1630,
         "video":false,
         "poster_path":"/3nk9UoepYmv1G9oP18q6JJCeYwN.jpg",
         "id":503919,
         "adult":false,
         "backdrop_path":"/5BmcysaAASA00FM0gRjD0ClMUY9.jpg",
         "original_language":"en",
         "original_title":"The Lighthouse",
         "genre_ids":[
            18,
            14,
            27,
            9648
         ],
         "title":"The Lighthouse",
         "vote_average":7.6,
         "overview":"Two lighthouse keepers try to maintain their sanity while living on a remote and mysterious New England island in the 1890s.",
         "release_date":"2019-10-18"
      },
      {
         "popularity":26.074,
         "vote_count":348,
         "video":false,
         "poster_path":"/uHpHzbHLSsVmAuuGuQSpyVDZmDc.jpg",
         "id":556678,
         "adult":false,
         "backdrop_path":"/5GbkL9DDRzq3A21nR7Gkv6cFGjq.jpg",
         "original_language":"en",
         "original_title":"Emma.",
         "genre_ids":[
            35,
            18,
            10749
         ],
         "title":"Emma.",
         "vote_average":7,
         "overview":"In 1800s England, a well-meaning but selfish young woman meddles in the love lives of her friends.",
         "release_date":"2020-02-13"
      }
   ],
   "page":1,
   "total_results":592,
   "dates":{
      "maximum":"2020-06-22",
      "minimum":"2020-05-05"
   },
   "total_pages":30
}
"""
let mockDataForDetailResponse = """
{
   "adult":false,
   "backdrop_path":"/jZ4YdDCnfK4cO13do9J4JaT8d2O.jpg",
   "belongs_to_collection":null,
   "budget":4000000,
   "genres":[
      {
         "id":53,
         "name":"Thriller"
      },
      {
         "id":878,
         "name":"Science Fiction"
      },
      {
         "id":27,
         "name":"Horror"
      }
   ],
   "homepage":"",
   "id":458305,
   "imdb_id":"tt8368406",
   "original_language":"en",
   "original_title":"Vivarium",
   "overview":"A young woman and her fiancé are in search of the perfect starter home. After following a mysterious real estate agent to a new housing development, the couple finds themselves trapped in a maze of identical houses and forced to raise an otherworldly child.",
   "popularity":43.951,
   "poster_path":"/myf3qzpeN0JbuFRPwSpJcz7rmAT.jpg",
   "production_companies":[
      {
         "id":18078,
         "logo_path":null,
         "name":"Fantastic Films",
         "origin_country":"IE"
      },
      {
         "id":49473,
         "logo_path":"/d0Cld16NzSAizrXfO6jrN3nH6az.png",
         "name":"Frakas Productions",
         "origin_country":"BE"
      },
      {
         "id":117433,
         "logo_path":null,
         "name":"PingPongFilm",
         "origin_country":""
      },
      {
         "id":111422,
         "logo_path":"/6H3JNNi6bFy7RAMYcTHbf0uSozR.png",
         "name":"Lovely Productions",
         "origin_country":"IE"
      },
      {
         "id":12142,
         "logo_path":"/rPnEeMwxjI6rYMGqkWqIWwIJXxi.png",
         "name":"XYZ Films",
         "origin_country":"US"
      }
   ],
   "production_countries":[
      {
         "iso_3166_1":"BE",
         "name":"Belgium"
      },
      {
         "iso_3166_1":"DK",
         "name":"Denmark"
      },
      {
         "iso_3166_1":"IE",
         "name":"Ireland"
      }
   ],
   "release_date":"2019-07-12",
   "revenue":0,
   "runtime":97,
   "spoken_languages":[
      {
         "iso_639_1":"en",
         "name":"English"
      }
   ],
   "status":"Released",
   "tagline":"You're home. Forever.",
   "title":"Vivarium",
   "video":false,
   "vote_average":5.9,
   "vote_count":419
}
"""
let mockDataForFailResponse = """
{
   "status_code":7,
   "status_message":"Invalid API key: You must be granted a valid key.",
   "success":false
}
"""

class MockMoviesListData {
    func getMockData() -> MoviesList? {
        let moviesList = try? JSONDecoder().decode(MoviesList.self, from: mockDataForMoviesList.data(using: .utf8)!)
        return moviesList
    }
}
